#!/usr/bin/env python
# -*- coding: utf-8 -*-


from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import QPixmap
import sys

import serial

import json
import time, uuid, urllib, urllib2
import hmac, hashlib
from base64 import b64encode


class SerialPortManager:

    def __init__(self):
        self.port = serial.Serial('/dev/ttyMCC',115200,timeout=1)

    def write(self, message):
        print("Writing data to serial port: " + message)
        self.port.write(message)
        self.port.flushOutput()

    def readx(self):
        return self.port.readline()

    def close(self):
        return self.port.close()


thoitiet = ['vòi rồng'		,
"bão nhiệt đới"			,
"bão"				,
"giông bão nghiêm trọng	"	,
"giông bão	"	,
"mưa và tuyết	"	,
"mưa có tuyết	"	,
"hỗn hợp tuyết và mưa đá	"	,
"mưa phùn lạnh giá	"	,
"mưa phùn	"	,
"mưa lạnh	"	,
"vòi hoa sen	"	,
"vòi hoa sen	"	,
"mưa tuyết	"	,
"mưa tuyết nhẹ	"	,
"thổi tuyết	"	,
"tuyết	"	,
"kêu	"	,
"ngủ	"	,
"bụi bặm	"	,
"có sương mù	"	,
"sương mù	"	,
"khói	"	,
"lúng túng	"	,
"gió	"	,
"lạnh	"	,
"Nhiều mây	"	,
"trời nhiều mây	"	,
"trời nhiều mây	"	,
"một phần mây (đêm)	"	,
"một phần mây (ngày)	"	,
"đêm rõ ràng)	"	,
"nắng	"	,
"hội chợ (đêm)	"	,
"công bằng (ngày)	"	,
"mưa hỗn hợp và mưa đá	"	,
"nóng bức	"	,
"giông bão bị cô lập	"	,
"sấm chớp rải rác	"	,
"sấm chớp rải rác	"	,
"mưa rào rải rác	"	,
"tuyết rơi nhiều	"	,
"Mưa tuyết rải rác	"	,
"tuyết rơi nhiều	"	,
"một phần mây	"	,
"mưa dông	"	,
"mưa tuyết	"	,
"sấm sét bị cô lập	",
];

class weatherStationForm(QtGui.QWidget):
    def __init__(self):
        super(weatherStationForm, self).__init__()
        self.ui = uic.loadUi('ws_ui.ui', self)

        #self.Form.setStyleSheet("background-image: url(resource/theme.png)")
        self.pushButtonReload.clicked.connect(self.onClick)
        
        self.port = serial.Serial('/dev/ttyMCC',115200,timeout=1)
        
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.showTime)
        timer.start(1000)
        self.showTime()
        self.getData()
        self.getDht()
        date = QtCore.QDate.currentDate()
        text = date.toString("dd-MM-yyyy")
        self.lcdNumber.display(text)
        self.label.setText(date.toString("dddd"))



    @QtCore.pyqtSlot(int)
    def onClick(self):

        self.getData()

    def showTime(self):
        time = QtCore.QTime.currentTime()
        text = time.toString('hh:mm')
        if (time.second() % 2) == 0:
            text = text[:2] + ' ' + text[3:]
        self.lcdNumber_2.display(text)
        if (time.second() % 5) == 0:
            self.getDht()

    def getDht(self):
        line = str(self.port.readline())
        if line:
            if (line[0]=='T'):
                nhietDo = line[1:3]
                doAm = line[3:5]
                self.lcdNumber_3.display(nhietDo)
                self.lcdNumber_4.display(doAm)

    def getData(self):
        thanhpho = str(self.lineEdit.text())

        str_thanhpho = thanhpho + ',ca'


        url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss'
        url = str(url)
        method = 'GET'
        app_id = '6Sv5Ve54'
        consumer_key = 'dj0yJmk9Z0ZSNTVueWVTcTZIJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTE0'
        consumer_secret = '1d65b120350e6e5710483e898a72e6a0eac61cfb'
        concat = '&'
        query = {'location': str_thanhpho, 'format': 'json', 'u': 'c'}
        oauth = {
            'oauth_consumer_key': consumer_key,
            'oauth_nonce': uuid.uuid4().hex,
            'oauth_signature_method': 'HMAC-SHA1',
            'oauth_timestamp': str(int(time.time())),
            'oauth_version': '1.0'
        }


        """
        Prepare signature string (merge all params and SORT them)
        """
        merged_params = query.copy()
        merged_params.update(oauth)
        sorted_params = [str(k) + '=' + str(urllib.quote(merged_params[k], safe='')) for k in sorted(merged_params.keys())]
        signature_base_str =  method + concat + urllib.quote(url, safe='') + concat + urllib.quote(concat.join(sorted_params), safe='')

        """
        Generate signature
        """
        composite_key = urllib.quote(consumer_secret, safe='') + concat
        oauth_signature = b64encode(hmac.new(composite_key, signature_base_str, hashlib.sha1).digest())

        """
        Prepare Authorization header
        """
        oauth['oauth_signature'] = oauth_signature
        auth_header = 'OAuth ' + ', '.join(['{}="{}"'.format(k,v) for k,v in oauth.iteritems()])

        """
        Send request
        """
        url = url + '?' + urllib.urlencode(query)
        request = urllib2.Request(url)
        request.add_header('Authorization', auth_header)
        request.add_header('X-Yahoo-App-Id', app_id)
        response = urllib2.urlopen(request).read()


        ketqua = json.loads(response)

        infor = "Bạn đang chọn thành phố : ".decode('utf_8')
        infor+= ketqua["location"]["city"]
        self.labelInfor.setText(infor)

        forecast = 0
        url = 'resource/png/' + str(ketqua["current_observation"]["condition"]["code"]) + '.png'
        pixmap = QPixmap(url)
        self.label_19.setPixmap(pixmap)
        self.label_20.setText(thoitiet[ketqua["current_observation"]["condition"]["code"]].decode('utf_8'))
        self.label_22.setText("Nhiệt độ: ".decode('utf_8')+str(ketqua["forecasts"][forecast]["low"])+"-"+str(ketqua["forecasts"][forecast]["high"])+"°C".decode('utf_8'))

        if (ketqua["current_observation"]["wind"]["speed"]<19):
            self.label_21.setText("Gió nhẹ ".decode('utf_8'))
        else:
            self.label_21.setText("Gió lớn ".decode('utf_8'))
        forecast = 1
        self.label_3.setText(str(ketqua["forecasts"][forecast]["day"])+"day: "+str(ketqua["forecasts"][forecast]["low"])+"-"+str(ketqua["forecasts"][forecast]["high"])+"°C".decode('utf_8'))
        url = 'resource/png/' + str(ketqua["forecasts"][forecast]["code"]) + '.png'
        pixmap = QPixmap(url)
        pixmap_resized = pixmap.scaled(64, 64)
        self.label_2.setPixmap(pixmap_resized)
        self.label_4.setText(thoitiet[ketqua["forecasts"][forecast]["code"]].decode('utf_8'))

        forecast = 2
        self.label_6.setText(str(ketqua["forecasts"][forecast]["day"])+"day: "+str(ketqua["forecasts"][forecast]["low"])+"-"+str(ketqua["forecasts"][forecast]["high"])+"°C".decode('utf_8'))
        url = 'resource/png/' + str(ketqua["forecasts"][forecast]["code"]) + '.png'
        pixmap = QPixmap(url)
        pixmap_resized = pixmap.scaled(64, 64)
        self.label_5.setPixmap(pixmap_resized)
        self.label_7.setText(thoitiet[ketqua["forecasts"][forecast]["code"]].decode('utf_8'))

        forecast = 3
        self.label_9.setText(str(ketqua["forecasts"][forecast]["day"])+"day: "+str(ketqua["forecasts"][forecast]["low"])+"-"+str(ketqua["forecasts"][forecast]["high"])+"°C".decode('utf_8'))
        url = 'resource/png/' + str(ketqua["forecasts"][forecast]["code"]) + '.png'
        pixmap = QPixmap(url)
        pixmap_resized = pixmap.scaled(64, 64)
        self.label_8.setPixmap(pixmap_resized)
        self.label_10.setText(thoitiet[ketqua["forecasts"][forecast]["code"]].decode('utf_8'))

        forecast = 4
        self.label_12.setText(str(ketqua["forecasts"][forecast]["day"])+"day: "+str(ketqua["forecasts"][forecast]["low"])+"-"+str(ketqua["forecasts"][forecast]["high"])+"°C".decode('utf_8'))
        url = 'resource/png/' + str(ketqua["forecasts"][forecast]["code"]) + '.png'
        pixmap = QPixmap(url)
        pixmap_resized = pixmap.scaled(64, 64)
        self.label_11.setPixmap(pixmap_resized)
        self.label_13.setText(thoitiet[ketqua["forecasts"][forecast]["code"]].decode('utf_8'))

        forecast = 5
        self.label_15.setText(str(ketqua["forecasts"][forecast]["day"])+"day: "+str(ketqua["forecasts"][forecast]["low"])+"-"+str(ketqua["forecasts"][forecast]["high"])+"°C".decode('utf_8'))
        url = 'resource/png/' + str(ketqua["forecasts"][forecast]["code"]) + '.png'
        pixmap = QPixmap(url)
        pixmap_resized = pixmap.scaled(64, 64)
        self.label_14.setPixmap(pixmap_resized)
        self.label_16.setText(thoitiet[ketqua["forecasts"][forecast]["code"]].decode('utf_8'))

app = QtGui.QApplication(sys.argv)
wsf = weatherStationForm()
#wsf.setAutoFillBackground(False)

wsf.groupBox_8.setStyleSheet("background-color: QColor(255,0,255,255);")
wsf.groupBox.setStyleSheet("background-color: darkCyan;")
wsf.groupBox_2.setStyleSheet("background-color: darkCyan;")
wsf.groupBox_3.setStyleSheet("background-color: darkCyan;")
wsf.groupBox_4.setStyleSheet("background-color: darkCyan;")
wsf.groupBox_5.setStyleSheet("background-color: darkCyan;")
wsf.groupBox_6.setStyleSheet("background-color: darkCyan;")
wsf.groupBox_7.setStyleSheet("background-color: darkCyan;")
#wsf.groupBox_8.setStyleSheet("background-image: url(resource/theme.png)")
#wsf.setStyleSheet("background-color: green;")
#wsf.setStyleSheet("QWidget {background-image: url(resource/theme.png)}")
wsf.show()
sys.exit(app.exec_())

# -*- coding: utf-8 -*-

"""
Weather API Python sample code
Copyright 2019 Oath Inc. Licensed under the terms of the zLib license see https://opensource.org/licenses/Zlib for terms.
$ python --version
Python 2.7.10
"""

import json
import time, uuid, urllib, urllib2
import hmac, hashlib
from base64 import b64encode


thoitiet = ['vòi rồng'		,
"	bão nhiệt đới"			,
"	bão"				,
"	giông bão nghiêm trọng	"	,
"	giông bão	"	,
"	mưa và tuyết	"	,
"	mưa có tuyết	"	,
"	hỗn hợp tuyết và mưa đá	"	,
"	mưa phùn lạnh giá	"	,
"	mưa phùn	"	,
"	mưa lạnh	"	,
"	vòi hoa sen	"	,
"	vòi hoa sen	"	,
"	mưa tuyết	"	,
"	mưa tuyết nhẹ	"	,
"	thổi tuyết	"	,
"	tuyết	"	,
"	kêu	"	,
"	ngủ	"	,
"	bụi bặm	"	,
"	có sương mù	"	,
"	sương mù	"	,
"	khói	"	,
"	lúng túng	"	,
"	gió	"	,
"	lạnh	"	,
"	Nhiều mây	"	,
"	trời nhiều mây	"	,
"	trời nhiều mây	"	,
"	một phần mây (đêm)	"	,
"	một phần mây (ngày)	"	,
"	đêm rõ ràng)	"	,
"	nắng	"	,
"	hội chợ (đêm)	"	,
"	công bằng (ngày)	"	,
"	mưa hỗn hợp và mưa đá	"	,
"	nóng bức	"	,
"	giông bão bị cô lập	"	,
"	sấm chớp rải rác	"	,
"	sấm chớp rải rác	"	,
"	mưa rào rải rác	"	,
"	tuyết rơi nhiều	"	,
"	Mưa tuyết rải rác	"	,
"	tuyết rơi nhiều	"	,
"	một phần mây	"	,
"	mưa dông	"	,
"	mưa tuyết	"	,
"	sấm sét bị cô lập	",
];


"""
Basic info
"""
print ("nhap thanh pho :")
thanhpho = raw_input()

str_thanhpho = thanhpho + ',ca'


url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss'
method = 'GET'
app_id = '6Sv5Ve54'
consumer_key = 'dj0yJmk9Z0ZSNTVueWVTcTZIJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTE0'
consumer_secret = '1d65b120350e6e5710483e898a72e6a0eac61cfb'
concat = '&'
query = {'location': str_thanhpho, 'format': 'json', 'u': 'c'}
oauth = {
    'oauth_consumer_key': consumer_key,
    'oauth_nonce': uuid.uuid4().hex,
    'oauth_signature_method': 'HMAC-SHA1',
    'oauth_timestamp': str(int(time.time())),
    'oauth_version': '1.0'
}


"""
Prepare signature string (merge all params and SORT them)
"""
merged_params = query.copy()
merged_params.update(oauth)
sorted_params = [k + '=' + urllib.quote(merged_params[k], safe='') for k in sorted(merged_params.keys())]
signature_base_str =  method + concat + urllib.quote(url, safe='') + concat + urllib.quote(concat.join(sorted_params), safe='')

"""
Generate signature
"""
composite_key = urllib.quote(consumer_secret, safe='') + concat
oauth_signature = b64encode(hmac.new(composite_key, signature_base_str, hashlib.sha1).digest())

"""
Prepare Authorization header
"""
oauth['oauth_signature'] = oauth_signature
auth_header = 'OAuth ' + ', '.join(['{}="{}"'.format(k,v) for k,v in oauth.iteritems()])

"""
Send request
"""
url = url + '?' + urllib.urlencode(query)
request = urllib2.Request(url)
request.add_header('Authorization', auth_header)
request.add_header('X-Yahoo-App-Id', app_id)
response = urllib2.urlopen(request).read()


ketqua = json.loads(response)

print "***********************************"
print '  thoi tiet luc nay tai ',ketqua["location"]["city"], ":"

print "    Thoi tiet: ",thoitiet[ketqua["current_observation"]["condition"]["code"]].decode('utf_8')
print "    Nhiet do: ",ketqua["current_observation"]["condition"]["temperature"]


for forecast in range(5):
	print "***********************************"
	print "  du bao thoi tiet ",ketqua["forecasts"][forecast]["day"],"day"
	print "    Thoi tiet: ",thoitiet[ketqua["forecasts"][forecast]["code"]].decode('utf_8')
	print "    Nhiet do: ",ketqua["forecasts"][forecast]["low"],"-",ketqua["forecasts"][forecast]["high"]
	